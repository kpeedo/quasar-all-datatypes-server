#!/bin/bash

echo 'creating quasar-all-datatypes-server.TEST directory (deletes any existing quasar-all-datatypes-server.TEST)'
rm -rf quasar-all-datatypes-server.TEST
rm -rf quasar-all-datatypes-server.TEST.tar.gz
mkdir quasar-all-datatypes-server.TEST

echo 'making required sub-dirs under quasar-all-datatypes-server.TEST'
mkdir quasar-all-datatypes-server.TEST/bin
mkdir quasar-all-datatypes-server.TEST/Configuration
 
echo 'copying files from dev environment'
cp bin/OpcUaServer quasar-all-datatypes-server.TEST/bin/
cp bin/ServerConfig.xml quasar-all-datatypes-server.TEST/bin/
cp bin/config.xml quasar-all-datatypes-server.TEST/bin/
cp Configuration/Configuration.xsd quasar-all-datatypes-server.TEST/Configuration/
 
echo 'dumping some quick n dirty examples in the README file'
touch quasar-all-datatypes-server.TEST/README
echo 'quick n dirty release, instructions' >> quasar-all-datatypes-server.TEST/README
echo '* cd quasar-all-datatypes-server.TEST/bin/' >> quasar-all-datatypes-server.TEST/README
echo '*./OpcUaServer -help' >> quasar-all-datatypes-server.TEST/README
echo '*./OpcUaServer -version' >> quasar-all-datatypes-server.TEST/README
echo '===========================' >> quasar-all-datatypes-server.TEST/README
echo 'and to run properly, use...' >> quasar-all-datatypes-server.TEST/README
echo './OpcUaServer'  >> quasar-all-datatypes-server.TEST/README
echo 'to start the server incremetn thread (periodically increments the floats, ints...) set item Controller.runIncrementer to TRUE' >> quasar-all-datatypes-server.TEST/README

echo 'does the following tree and README look right?'
tree quasar-all-datatypes-server.TEST/
cat quasar-all-datatypes-server.TEST/README

echo 'compressing tree to archive'
tar -zcvf quasar-all-datatypes-server.TEST.tar.gz quasar-all-datatypes-server.TEST/
 
echo 'if that looks good then you need to...'
echo '* copy compressed file to target location'
echo '* unzip compressed file: tar -zxvf quasar-all-datatypes-server.TEST.tar.gz'
echo '* follow the instructions in quasar-all-datatypes-server.TEST/README'
