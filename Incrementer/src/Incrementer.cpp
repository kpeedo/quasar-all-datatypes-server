/*
 * Incrementer.cpp
 *
 *  Created on: Oct 31, 2016
 *      Author: bfarnham
 */

#include "Incrementer.h"
#include <set>
#include <boost/thread.hpp>
#include <LogIt.h>

using std::set;

Incrementer::Incrementer(Device::DController* controller, const set<Device::DMultiProperty*>& multiProperties)
:m_controller(controller), m_multiProperties(multiProperties), m_scheduleStopRunning(false), m_isRunning(false), m_incrementPeriodMs(1000), m_incrementCounter(0)
{}

Incrementer::~Incrementer()
{
	stopIncrementing();
}

void Incrementer::startIncrementing()
{
	LOG(Log::INF) << __FUNCTION__ << " called, starting increment thread, current status running ? ["<<(m_isRunning?'Y':'N')<<"] stop scheduled ? ["<<(m_scheduleStopRunning?'Y':'N')<<"]";
	if(m_isRunning || m_scheduleStopRunning) return; // ignore.

	m_scheduleStopRunning = false;
	m_incrementerThread = boost::thread(boost::ref(*this));
}

void Incrementer::stopIncrementing()
{
	LOG(Log::INF) << __FUNCTION__ << " called, stopping increment thread, current status running ? ["<<(m_isRunning?'Y':'N')<<"] stop scheduled ? ["<<(m_scheduleStopRunning?'Y':'N')<<"]";
	if(!m_isRunning) return; // ignore
	if(m_scheduleStopRunning) return; // ignore

	m_scheduleStopRunning = true;
	m_incrementerThread.join();

	m_isRunning = false;
	m_scheduleStopRunning = false;
}

void Incrementer::setIncrementPeriodMs(const uint32_t& incrementPeriodMs)
{
	m_incrementPeriodMs = incrementPeriodMs;
	LOG(Log::INF) << __FUNCTION__ << " called, new increment period ["<<m_incrementPeriodMs<<"ms]";
}

/**
 * used for main thread function
 */
void Incrementer::operator()()
{
	LOG(Log::INF) << __FUNCTION__ << " increment thread - start, current increment period ["<<m_incrementPeriodMs<<"ms]";
	m_isRunning = true;
	while(!m_scheduleStopRunning)
	{
		m_controller->updateIncrementCounter(++m_incrementCounter);
		for(set<Device::DMultiProperty*>::const_iterator it = m_multiProperties.begin(); it != m_multiProperties.end(); ++it)
		{
			(*it)->increment();
		}
		boost::this_thread::sleep_for(boost::chrono::milliseconds(m_incrementPeriodMs));
	}
	LOG(Log::INF) << __FUNCTION__ << " increment thread - end";
}
