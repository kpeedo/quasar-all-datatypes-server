/*
 * Incrementer.h
 *
 *  Created on: Oct 31, 2016
 *      Author: bfarnham
 */

#ifndef INCREMENTER_INCLUDE_INCREMENTER_H_
#define INCREMENTER_INCLUDE_INCREMENTER_H_

#include <set>
#include <boost/thread.hpp>
#include "DMultiProperty.h"
#include "DController.h"

class Incrementer
{
public:
	Incrementer(Device::DController* controller, const std::set<Device::DMultiProperty*>& multiProperties);
	virtual ~Incrementer();

	void setIncrementPeriodMs(const uint32_t& incrementPeriodMs);
	void startIncrementing();
	void stopIncrementing();

	/**
	 * used for main thread function
	 */
	void operator()();

private:

	Device::DController* m_controller;
	const std::set<Device::DMultiProperty*> m_multiProperties;
	boost::thread m_incrementerThread;

	bool m_scheduleStopRunning;
	bool m_isRunning;
	uint32_t m_incrementPeriodMs;
	uint32_t m_incrementCounter;
};

#endif /* INCREMENTER_INCLUDE_INCREMENTER_H_ */
